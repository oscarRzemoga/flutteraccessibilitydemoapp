class Service {
  final String name;
  final String serviceDescription;
  final String imageUrl;

  Service({
    this.name,
    this.serviceDescription,
    this.imageUrl,
  });

  static List<Service> dummyActivities() {
    var hiking = Service(
        name: "Hiking",
        serviceDescription: "Hiking with friends",
        imageUrl: "asset/images/hiking.jpg");
    var climbing = Service(
        name: "Climbing",
        serviceDescription: "Climbing as high as you can",
        imageUrl: "asset/images/climbing.jpg");
    return [hiking, climbing];
  }

  static List<Service> dummyMeals() {
    var breakfast = Service(
        name: "American breakfast",
        serviceDescription: "Enjoy your first meal of the day",
        imageUrl: "asset/images/american.jpg");
    var paella = Service(
        name: "Paella Valenciana",
        serviceDescription: "A traditional spanish meal",
        imageUrl: "asset/images/paella-valenciana.png");
    return [breakfast, paella];
  }
}