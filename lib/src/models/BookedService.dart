class BookedService {
  final String title;
  final String schedule;
  final String location;
  final String startDate;
  final String endDate;
  final String mainDescription;

  BookedService({
    this.title,
    this.schedule,
    this.location,
    this.startDate,
    this.endDate,
    this.mainDescription,
  });

  static List<BookedService> dummyServices() {
    var bookedService = BookedService(
        title: "SCUBA Diving",
        schedule: "7AM - 3PM",
        location: "Central Park",
        startDate: "July 30, 2022 08:00",
        endDate: "July 30, 2022 13:00",
        mainDescription: "Don't forget to bring your clothes and food to this marvelous day"
    );
    return [bookedService];
  }
}