import 'package:accessibilitydemoapp/src/presentation/home_view.dart';
import 'package:flutter/material.dart';

class AccessibilityApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomeView(),
    );
  }
}