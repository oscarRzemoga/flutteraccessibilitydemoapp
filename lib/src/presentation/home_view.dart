import 'package:accessibilitydemoapp/src/models/Service.dart';
import 'package:accessibilitydemoapp/src/models/BookedService.dart';
import 'package:flutter/material.dart';
import 'package:supercharged/supercharged.dart';

class HomeView extends StatelessWidget {
  final _activities = Service.dummyActivities();
  final _meals = Service.dummyMeals();
  final _bookedServices = BookedService.dummyServices();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: "#f0f0f2".toColor(),
      body: SafeArea(
        top: false,
        child: CustomScrollView(
          semanticChildCount: 4,
          slivers: <Widget>[
            SliverAppBar(
              title: Text("Reservation Schedule"),
              pinned: true,
            ),
            _buildAvailableDaySection(context),
            SliverList(
              delegate: _buildAvailableActivitiesSliverChildBuilderDelegate(),
            ),
            SliverList(
              delegate: _buildAvailableMealsSliverChildBuilderDelegate(),
            ),
            SliverList(
              delegate: _buildBookedServicesSilverChildBuilderDelegate(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAvailableDaySection(BuildContext context) {
    return SliverToBoxAdapter(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 10.0),
        child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          Padding(
            padding: EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Semantics(
                  namesRoute: true,
                  child:
                      Text("DAY"),
                  header: true),
            ),
          ),
          Semantics(
            label: "Available day to reservation Monday.",
            child: Container(
              color: Colors.white,
              height: 65.0,
              child: ExcludeSemantics(child: Center(child: Text("Monday"))),
            ),
          ),
        ]),
      ),
    );
  }

  Widget _buildAvailableServicesSection(BuildContext context, Service service,
      String title, String accessibilityLabel, bool showHeader) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
        if (showHeader)
          Padding(
            padding: EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
            child: Align(
              alignment: Alignment.centerLeft,
              child: Semantics(
                  namesRoute: true,
                  child:
                      Text(title),
                  header: true),
            ),
          ),
        MergeSemantics(
            child: Semantics(
          label: accessibilityLabel,
          child: Container(
            color: Colors.white,
            height: 90.0,
            child: ExcludeSemantics(
                child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(20.0, 5.0, 5.0, 5.0),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(4.0),
                          child: Image.asset(service.imageUrl,
                              height: 80, width: 80, fit: BoxFit.cover)),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(5.0, 0.0, 5.0, 0.0),
                      child: Wrap(
                        direction: Axis.vertical,
                        spacing: 10,
                        children: <Widget>[
                          Text(service.name),
                          Text(service.serviceDescription),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            )),
          ),
        )),
      ]),
    );
  }

  Widget _buildBookedServicesSection(
      BuildContext context,
      BookedService service,
      String headerTitle,
      String accessibilityLabel,
      bool showHeader) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
        if (showHeader)
          Padding(
            padding: EdgeInsets.only(left: 10.0, top: 5.0, bottom: 5.0),
            child: Align(
                alignment: Alignment.centerLeft,
                child: Text(headerTitle)),
          ),
        Semantics(
            explicitChildNodes: true,
            label: accessibilityLabel,
            child: Container(
                color: Colors.white,
                height: 250.0,
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ExcludeSemantics(
                                child: Text(service.title),
                            ),
                            Semantics(
                              explicitChildNodes: true,
                              label: "Reservation aditional information.",
                              child: IconButton(
                                icon: Icon(
                                  Icons.info,
                                  color: Colors.blueAccent,
                                  size: 30.0,
                                ),
                                onPressed: () {
                                  print("something");
                                },
                              ),
                            ),
                          ]),
                    ),
                    ExcludeSemantics(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Schedule"),
                              Text(service.schedule),
                            ]),
                      ),
                    ),
                    ExcludeSemantics(
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("Location"),
                              Text(service.location),
                            ]),
                      ),
                    ),
                    ExcludeSemantics(
                      child: Padding(
                        padding: EdgeInsets.all(20.0),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(child: Text(service.mainDescription)),
                            ]),
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Semantics(
                        explicitChildNodes: true,
                        child: RaisedButton(
                          child: Text('Manage reservation'),
                          onPressed: () {},
                          color: Colors.blueAccent,
                        ),
                      ),
                    ),
                  ],
                ),
          ),
        ),
      ]),
    );
  }

  SliverChildBuilderDelegate
      _buildAvailableActivitiesSliverChildBuilderDelegate() {
    return SliverChildBuilderDelegate(
      (context, index) {
        final currentActivityCount = (index + 1).toString();
        final totalActivitiesCount = (_activities.length).toString();
        final activity = _activities[index];
        final headerTitle = "AVAILABLE ACTIVITIES";
        final accessibilityLabel =
            "Activity $currentActivityCount of $totalActivitiesCount, ${activity.name},  ${activity.serviceDescription}";

        return _buildAvailableServicesSection(context, activity, headerTitle,
            accessibilityLabel, index == 0 ? true : false);
      },
      childCount: _activities.length,
      semanticIndexOffset: 2,
    );
  }

  SliverChildBuilderDelegate _buildAvailableMealsSliverChildBuilderDelegate() {
    return SliverChildBuilderDelegate(
      (context, index) {
        final currentMealCount = index + 1;
        final totalMealsCount = _meals.length;
        final meal = _meals[index];
        final headerTitle = "AVAILABLE MEALS";
        final accessibilityLabel =
            "Meal $currentMealCount of $totalMealsCount, ${meal.name},  ${meal.serviceDescription}";

        return _buildAvailableServicesSection(context, meal, headerTitle,
            accessibilityLabel, index == 0 ? true : false);
      },
      childCount: _meals.length,
      semanticIndexOffset: _activities.length,
    );
  }

  SliverChildBuilderDelegate _buildBookedServicesSilverChildBuilderDelegate() {
    return SliverChildBuilderDelegate(
      (context, index) {
        final currentBookedServiceCount = index + 1;
        final totalBookedServicesCount = _bookedServices.length;
        final bookService = _bookedServices[index];
        final headerTitle = "BOOKED SERVICES";
        final accessibilityLabel = "Reservation $currentBookedServiceCount of $totalBookedServicesCount, "
            "activity schedule from ${bookService.startDate} to ${bookService.endDate}, meeting point ${bookService.location}, ${bookService.mainDescription}   ";
        return _buildBookedServicesSection(context, _bookedServices[index],
            headerTitle, accessibilityLabel, true);
      },
      childCount: _bookedServices.length,
      semanticIndexOffset: _meals.length + _activities.length,
    );
  }
}